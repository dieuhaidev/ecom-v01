<?php

return [
    
   'left-menu' => [
      [
         'name' => 'Trang Chủ',
         'icon' => 'fa-edit',
         'route'=>'/admin'
      ],
      [
      'name' => 'Bài viết',
      'icon' => 'fa-edit',
      'route'=>'/admin/blog',
      'items'=>[
            [
               'name' => 'Tất cả bài viết',
               'icon' => 'fa-edit',
               'route'=>'/admin/blog/list',
            ],
            [
               'name' => 'Bài viết mới',
               'icon' => 'fa-edit',
               'route'=>'/admin/blog/create',
            ],
            [
               'name' => 'Chuyên mục',
               'icon' => 'fa-edit',
               'route'=>'/admin/blog/category',
            ],
            [
               'name' => 'Thẻ',
               'icon' => 'fa-edit',
               'route'=>'/admin/blog/tags',
            ]
            
         ]
   ],
   [
      'name' => 'Đơn hàng',
      'icon' => 'fa-edit',
      'route'=>'category',
      'items'=>[
            [
               'name' => 'Tổng quan',
               'icon' => 'fa-edit',
               'route'=>'/admin/orders',
            ],
            [
               'name' => 'Đơn hàng',
               'icon' => 'fa-edit',
               'route'=>'/admin/orders/list',
            ],
            [
               'name' => 'Chuyên mục',
               'icon' => 'fa-edit',
               'route'=>'/admin/categories',
            ],
            [
               'name' => 'Thẻ',
               'icon' => 'fa-edit',
               'route'=>'category.list',
            ]
            
         ]
   ],
   [
      'name' => 'Sản phẩm',
      'icon' => 'fa-edit',
      'route'=>'category',
      'items'=>[
                  [
                     'name' => 'Tất cả sản phẩm',
                     'icon' => 'fa-edit',
                     'route'=>'/admin/products',
                  ],
                  [
                     'name' => 'Grid sản phẩm',
                     'icon' => 'fa-edit',
                     'route'=>'/admin/products/grid',
                  ],
                  [
                     'name' => 'Thêm mới',
                     'icon' => 'fa-edit',
                     'route'=>'/admin/products/create',
                  ],
                  [
                     'name' => 'Danh mục',
                     'icon' => 'fa-edit',
                     'route'=>'/admin/categories',
                  ],
                  [
                     'name' => 'Từ Khóa',
                     'icon' => 'fa-edit',
                     'route'=>'category.list',
                  ],
                  [
                     'name' => 'Các thuộc tính',
                     'icon' => 'fa-edit',
                     'route'=>'category.list',
                  ]
               ]
      ]
   ],
    'module_childrent' => [
        'list',
        'add',
        'edit',
        'delete'
    ]
];
