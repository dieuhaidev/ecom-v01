<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|max:50|min:1',
            'description'=>'max:500|min:20',
        ];
    }

    public function messages()
        {
            return [
                'name.required' => 'Không được phép để trống',
                'name.max' => 'Không được phép qua 50 kí tự',
                'name.min' => 'Không được phép ít hơn 1 kí tự',

    
                'description.max' => 'Không được phép qua 255 kí tự',
                'description.min' => 'Không được phép ít hơn 10 kí tự',
            ];
        }

}
