<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddCategoryRequest;
use App\Admin\Category;
use Yajra\Datatables\Datatables;

class CategoryController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     private $category;


     public function __construct(Category $category){
         $this->category = $category;

     }
     public function index()
     {  
        $categories  =$this->category->latest()->paginate(10);
         return view('admin.category.index',compact('categories'));
     }
     public function datatable(){
         return Datatables::of(Category::query())->make(true);
     }
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(AddCategoryRequest $request)
     {  

        if ($request->slug)
        {
            $slug = $request->slug;
        }else
        {
            $slug = str_slug($request->name);
        }
        $this->category->create([
            'name'=>$request->name,
            'description'=>$request->description,
            'feature_image_path'=>$request->feature_image_path,
            'parent_id'=>$request->parent_id,
            'slug'=>$slug,
        ]);
        session()->flash('success','Add to trash successfully');
        return redirect()->back();
     }
 
     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
        return view('admin.show');
     }
 
     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
        $category = $this->category::find($id);
        return view('admin.category.index',compact('category'));
     }
 
     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
     }
 
     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
     }
}
