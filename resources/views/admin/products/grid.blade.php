@extends('admin.master')
@section('title')
    <title>INSPINIA | Sản Phẩm</title>
@endsection
@push('css')
    <!-- Toastr style -->
    <link href="{{asset('')}}css/plugins/toastr/toastr.min.css" rel="stylesheet">
@endpush

@section('index')
    @include('admin.components.page-heading',['name'=>'Sản Phẩm','key'=>'Danh Sách'])
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
         <div class="col-lg-12">
             <div class="ibox float-e-margins">
                 <div class="ibox-title">
                     <h5>Autocomplete - Bootstrap Typehead</h5>

                     <div class="ibox-tools">
                         <a class="collapse-link">
                             <i class="fa fa-chevron-up"></i>
                         </a>
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                             <i class="fa fa-wrench"></i>
                         </a>
                         <ul class="dropdown-menu dropdown-user">
                             <li><a href="#">Config option 1</a>
                             </li>
                             <li><a href="#">Config option 2</a>
                             </li>
                         </ul>
                         <a class="close-link">
                             <i class="fa fa-times"></i>
                         </a>
                     </div>
                 </div>
                 <div class="ibox-content">
                     <strong>Autocomplete - typehead</strong><br/>
                     The Typeahead plugin from Twitter's Bootstrap 2 ready to use with Bootstrap 3 and Bootstrap 4. Full documentation can be found: <a href="https://github.com/bassjobsen/Bootstrap-3-Typeahead" target="_blank">https://github.com/bassjobsen/Bootstrap-3-Typeahead</a>
                 </div>
             </div>
         </div>
     </div>
      <div class="row">
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box active">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>






      </div>
      <div class="row">
         <div class="col-lg-12">
             <div class="ibox float-e-margins">
                 <div class="ibox-title">
                     <h5>Autocomplete - Bootstrap Typehead</h5>

                     <div class="ibox-tools">
                         <a class="collapse-link">
                             <i class="fa fa-chevron-up"></i>
                         </a>
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                             <i class="fa fa-wrench"></i>
                         </a>
                         <ul class="dropdown-menu dropdown-user">
                             <li><a href="#">Config option 1</a>
                             </li>
                             <li><a href="#">Config option 2</a>
                             </li>
                         </ul>
                         <a class="close-link">
                             <i class="fa fa-times"></i>
                         </a>
                     </div>
                 </div>
                 <div class="ibox-content">
                     <strong>Autocomplete - typehead</strong><br/>
                     The Typeahead plugin from Twitter's Bootstrap 2 ready to use with Bootstrap 3 and Bootstrap 4. Full documentation can be found: <a href="https://github.com/bassjobsen/Bootstrap-3-Typeahead" target="_blank">https://github.com/bassjobsen/Bootstrap-3-Typeahead</a>
                 </div>
             </div>
         </div>
     </div>
      <div class="row">
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

      </div>
      <div class="row">
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3">
              <div class="ibox">
                  <div class="ibox-content product-box">

                      <div class="product-imitation">
                          [ INFO ]
                      </div>
                      <div class="product-desc">
                          <span class="product-price">
                              $10
                          </span>
                          <small class="text-muted">Category</small>
                          <a href="#" class="product-name"> Product</a>



                          <div class="small m-t-xs">
                              Many desktop publishing packages and web page editors now.
                          </div>
                          <div class="m-t text-righ">

                              <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

      </div>




  </div>
@endsection

@push('js')
<script src="{{asset('')}}js/plugins/footable/footable.all.min.js"></script>

<!-- Page-Level Scripts -->
<script>


</script>
@endpush
