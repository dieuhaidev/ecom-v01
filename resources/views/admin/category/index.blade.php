@extends('admin.master')
@section('title')
    <title>INSPINIA | Danh mục</title>
@endsection
@push('css')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<style>
   .h_{
      /* display:none; */
   }
   .w_hover img{
       height: 30px;
   }
</style>
@endpush

@section('index')
    @include('admin.components.page-heading',['name'=>'Danh mục','key'=>'Danh Sách'])
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh mục sản phẩm</h5>
                    </div>
                    <div class="ibox-content">
                        <h3 class="no-margins">Thêm danh mục mới</h3>
                        <small>Danh mục sản phẩm cho cửa hàng của bạn được quản lý ở đây. Để thay đổi thứ tự danh mục hiển thị trên trang web, bạn có thể kéo thả chúng để sắp xếp. Để xem chi tiết các danh mục, nhấp vào liên kết "Tùy chọn màn hình" ở trên cùng của trang.</small>
                        <p>Sign in today for more expirience.</p>
                        <form role="form"  action="{{route('categories.store')}}" method="POST">@csrf
                            <div class="form-group @error('name') has-error @enderror "><label>Tên</label> 
                              <input name="name" value="{{ old('name')}}" type="text" placeholder="Tên danh mục" class="form-control">
                              @error('name')<small class="text-danger">{{ $message }}</small> <br>@enderror
                              <small>Tên riêng sẽ hiển thị trên trang mạng của bạn.</small>
                           </div>
                            
                            <div class="form-group "><label>Chuỗi cho đường dẫn tĩnh</label> 
                              <input type="text"  value="{{ old('slug')}}" name="slug" placeholder="Tên danh mục" class="form-control">
                              <small>Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</small>
                           </div>
                           <div class="form-group"><label>Danh mục cha</label> 
                              <select class="form-control m-b" name="parent_id" id="">
                                 <option value="0">Chọn danh mục cha</option>
                                 @foreach($categories as $category)
                                    <option value="{{$category->parent_id}}">{{$category->name}}</option>
                                 @endforeach
                              </select>
                              <small>Chỉ định một chuyên mục Cha để tạo thứ bậc.</small>
                           </div>
                           <div class="form-group @error('name') has-error @enderror"> <label>Mô tả</label>
                              <textarea class="form-control" name="description" cols="10" rows="5">{{ old('slug')}}</textarea>
                              @error('name')<small class="text-danger">{{ $message }}</small><br>@enderror
                              <small>Thông thường mô tả này không được sử dụng trong các giao diện, tuy nhiên có vài giao diện có thể hiển thị mô tả này.</small>
                           </div>
                           <div class="form-group"> <label>Hình thu nhỏ</label>
                              <input name="feature_image_path" type="file" class="form-control">
                           </div>
                            <div>
                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm danh mục</strong></button>
                                <h5>Danh mục sản phẩm</h5>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
               <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Danh Sách danh mục </h5>
                      <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                              <i class="fa fa-wrench"></i>
                          </a>
                          <ul class="dropdown-menu dropdown-user">
                              <li><a href="#">Config option 1</a>
                              </li>
                              <li><a href="#">Config option 2</a>
                              </li>
                          </ul>
                          <a class="close-link">
                              <i class="fa fa-times"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">
                      <div class="row">
                          <div class="col-sm-5 m-b-xs"><select class="input-sm form-control input-s-sm inline">
                              <option value="0">Option 1</option>
                              <option value="1">Option 2</option>
                              <option value="2">Option 3</option>
                              <option value="3">Option 4</option>
                          </select>
                          </div>
                          <div class="col-sm-4 m-b-xs">
                              <div data-toggle="buttons" class="btn-group">
                                  <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Day </label>
                                  <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                  <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>
                              </div>
                          </div>
                          <div class="col-sm-3">
                              <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                  <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                          </div>
                      </div>
                      <div class="table-responsive">
                          <table class="table table-striped" id="categories">
                              <thead>
                              <tr>

                                  <th>#</th>
                                  <th>Hình ảnh </th>
                                  <th>Tên </th>
                                  <th>Mô tả </th>
                                  <th>Chuổi tĩnh</th>
                                  <th>Xem</th>
                                  <th>Sửa</th>
                                  <th>Xóa</th>
                              </tr>
                              </thead>
                              <tbody >
                                  @foreach ($categories as $categoryItem)
                                    <tr class="w_hover">
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><img src="{{$categoryItem->feature_image_path}}" alt=""></td>
                                        <td>{{$categoryItem->name}}</td>
                                        <td><small>{{$categoryItem->description}}</small></td>
                                        <td><small>{{$categoryItem->slug}}</small></td>
                                        <td><a href="{{route('categories_edit',['id'=>$categoryItem->id])}}"><button type="button" class="btn btn-xs btn-success">Sửa</button></a></td>
                                        <td><button type="button" class="btn btn-xs btn-danger">Xóa</button></td>
                                    </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
           </div>
        </div>
    </div>
@endsection

@push('js')
   <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
@endpush
