<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'Quần Áo','description'=>'mô tả quần áo', 'feature_image_path'=>'https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-1/104430086_567512490614102_4104233533066563928_n.jpg?_nc_cat=109&_nc_sid=dbb9e7&_nc_ohc=gQGvqZ8W3cEAX_VsXAD&_nc_ht=scontent-sin6-2.xx&oh=5588336de782f4d9fade6e337232a5fc&oe=5F265D1B','parent_id'=>'0','slug'=>'quan-ao'],
            ['name' => 'Giày Dép','description'=>'mô tả quần áo', 'feature_image_path'=>'https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-1/104430086_567512490614102_4104233533066563928_n.jpg?_nc_cat=109&_nc_sid=dbb9e7&_nc_ohc=gQGvqZ8W3cEAX_VsXAD&_nc_ht=scontent-sin6-2.xx&oh=5588336de782f4d9fade6e337232a5fc&oe=5F265D1B', 'parent_id'=>'0', 'slug'=>'giay-dep'],
            ['name' => 'Nón Mũ','description'=>'mô tả quần áo', 'feature_image_path'=>'https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-1/104430086_567512490614102_4104233533066563928_n.jpg?_nc_cat=109&_nc_sid=dbb9e7&_nc_ohc=gQGvqZ8W3cEAX_VsXAD&_nc_ht=scontent-sin6-2.xx&oh=5588336de782f4d9fade6e337232a5fc&oe=5F265D1B', 'parent_id'=>'0', 'slug'=>'non-mu'],
            ['name' => 'Thời Trang Nam','description'=>'mô tả quần áo', 'feature_image_path'=>'https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-1/104430086_567512490614102_4104233533066563928_n.jpg?_nc_cat=109&_nc_sid=dbb9e7&_nc_ohc=gQGvqZ8W3cEAX_VsXAD&_nc_ht=scontent-sin6-2.xx&oh=5588336de782f4d9fade6e337232a5fc&oe=5F265D1B', 'parent_id'=>'1', 'slug'=>'thoi-trang-nam'],
            ['name' => 'Thời Trang Nữ','description'=>'mô tả quần áo', 'feature_image_path'=>'https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-1/104430086_567512490614102_4104233533066563928_n.jpg?_nc_cat=109&_nc_sid=dbb9e7&_nc_ohc=gQGvqZ8W3cEAX_VsXAD&_nc_ht=scontent-sin6-2.xx&oh=5588336de782f4d9fade6e337232a5fc&oe=5F265D1B', 'parent_id'=>'1', 'slug'=>'thoi-trang-nu'],
        ]);
    }
}
